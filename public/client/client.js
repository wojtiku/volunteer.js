(function() {
    "use strict";

    function isBrowserSupported() {
        return (
            (typeof Worker != "undefined") &&
            (typeof Blob != "undefined") &&
            (typeof URL || "undefined" != typeof webkitURL != "undefined")
        );
    }

    if (!isBrowserSupported()) {
        return;
    }

    var socket = io("/compute");
    var worker;
    var currentTaskId;
    var computations = {};

    socket.on("task", function(task) {
        currentTaskId = task.id;
        if (worker && worker.id == task.computation_id) {
            sendTaskToWorker(task);
        } else if (computations[task.computation_id]) {
            initWorker(task.computation_id);
            sendTaskToWorker(task);
        } else {
            socket.emit('get-computation', task.computation_id)
            socket.once('computation', function getComputation(computation) {
                computations[computation.id] = computation;
                initWorker(computation.id);
                sendTaskToWorker(task);
            })
        }
    });

    window.onerror = function (message, url) {
        console.log("socket", socket);
        if (socket) {
            var prefix = (url.indexOf("blob:") === 0) ? "Error processing computation task: " : "Error in execution script, contact administrator: ";
            socket.emit("err", {
                id: currentTaskId,
                error: prefix + message,
                duration: 0
            });
            console.log("emitted error", currentTaskId, prefix + message);
        }
    };

    function initWorker(id) {
        if (!computations[id]) return;

        var URL = window.URL || window.webkitURL;
        // Build a worker from an anonymous function body
        var blobURL = createWorkerBlob(computations[id]);

        if (worker) {
            worker.terminate();
            worker = undefined;
        }
        worker = new Worker( blobURL );

        worker.id = id;
        worker.onmessage = function (msg) {
            msg = JSON.parse(msg.data);
            if (msg.type == "result") {
                socket.emit("res", msg.result);
                console.log("emitted result", msg.result);
            } else if (msg.type == "error") {
                socket.emit("err", {
                    id: currentTaskId,
                    error: msg.result,
                    duration: msg.duration
                });
                console.log("emitted error", currentTaskId, msg.err);
            }
        };

        worker.onerror = function (err) {
            window.onerror(err.message, err.filename);
            worker.terminate();
            worker = undefined;
            return true;
        };
    }

    function sendTaskToWorker(task) {
        if (worker && worker.id == task.computation_id) {
            worker.postMessage(JSON.stringify({
                type: "task",
                task: task
            }));
        }
    }

    // create worker blob so we can spawn new WebWorker without new request
    function createWorkerBlob(computation) {
        return URL.createObjectURL(new Blob(
            [
                '(' +
                    runWorker.toString() +
                    ')(' + computation.id + ', (function returnWorker() {' +
                    '"use strict";\n' +
                    computation.code + '\n' +
                    'return compute;' +
                    '})())'
            ],
            { type: 'application/javascript' }
        ));
    }

    // this gets executed in the context of WebWorker
    function runWorker(id, compute) {
        if (typeof compute != "function") throw new Error("'compute' function not found");

        // Polyfill the User Timing API
        if (!self.performance) {
            self.performance = {};
        }
        if (!self.performance.now){
            var start = Date.now();
            if (self.performance.timing && self.performance.timing.navigationStart) {
                start = performance.timing.navigationStart;
            }
            self.performance.now = function now() {
                return Date.now() - start;
            }
        }

        function runComputation(task) {
            var start = self.performance.now();
            try {
                task.result = compute(task.data);
                task.duration = self.performance.now() - start;
                postMessage(JSON.stringify({
                    type: "result",
                    result: task
                }));
            } catch (err) {
                postMessage(JSON.stringify({
                    type: "error",
                    result: "Error processing computation task: " + err.toString(),
                    duration: self.performance.now() - start
                }));
            }
        }

        self.onmessage = function (msg) {
            msg = JSON.parse(msg.data);
            if (msg.type == "task") {
                if (msg.task.computation_id == id) {
                    runComputation(msg.task)
                } else {
                    throw "Wrong computation id!";
                }
            }
        };
    }
})();