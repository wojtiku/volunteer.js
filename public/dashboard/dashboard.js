/* Controllers */

// Declare app level module which depends on filters, and services
var app = angular.module('volounteerJsApp', ['ui.bootstrap']);

app.factory('socket', function ($rootScope) {
    var socket = io("/dashboard");
    return {
        on: function (eventName, callback) {
            socket.on(eventName, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    callback.apply(socket, args);
                });
            });
        },
        emit: function (eventName, data, callback) {
            socket.emit(eventName, data, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                    if (callback) {
                        callback.apply(socket, args);
                    }
                });
            })
        }
    };
});

app.controller('VolounteerJsDashboardCtrl', function VolounteerJsDashboardCtrl($scope, $rootScope, socket) {
    $scope.connectedWorkers = 0;
    $scope.maxCurrentlyComputedTasks = 0;
    $scope.currentlyComputedTasks = 0;
    $scope.computations = 0;

    socket.on('connected-workers', function onConnectedWorkers(connectedWorkers) {
        $scope.connectedWorkers = connectedWorkers;
    });

    socket.on('connected-workers', function onConnectedWorkers(connectedWorkers) {
        $scope.connectedWorkers = connectedWorkers;
    });

    socket.on('currently-computed-tasks', function onCurrentlyCoputedTasks(currentlyComputedTasks) {
        $scope.currentlyComputedTasks = currentlyComputedTasks;
        if (currentlyComputedTasks > $scope.maxCurrentlyComputedTasks) {
            $scope.maxCurrentlyComputedTasks = currentlyComputedTasks;
        }
    });

    socket.on('computations', function onComputations(computations) {
        $scope.computations = computations;
        $rootScope.$broadcast('new-computations');
    });

    $scope.resetMaxCurrentlyComputedTasks = function resetMaxCurrentlyComputedTasks() {
        $scope.maxCurrentlyComputedTasks = $scope.currentlyComputedTasks;
    }
});

app.controller('VolounteerJsComputationCtrl', function VolounteerJsComputationCtrl($scope, $http) {
    $scope.moment = moment;

    $scope.fetchRecentComputations = function fetchRecentComputations() {
        $http
            .get('/dashboard/recent-computations')
            .success(function onRecentComputationsXhr(data) {
                $scope.computations = data;
            });
    };
    $scope.$on('new-computations', function() {
        setTimeout($scope.fetchRecentComputations, 100);
    });
});