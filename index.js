var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var bodyParser = require('body-parser');
var cors = require('cors');
var regexpParam = require('./lib/regexp-param');
var dataDao = require('./dao/data');

//var pg = require('pg.js');
//var session = require("express-session");
//var pgSession = require('connect-pg-simple')(session);
//var sessionMiddleware = session({
//    store: new pgSession({
//        pg : pg,
//        conString : require('./config').db.connectionString()
//    }),
//    secret: "volounteer.js",
//    saveUninitialized: true,
//    resave: true
//});
//io.use(function(socket, next) { sessionMiddleware(socket.request, socket.request.res, next); });
//app.use(sessionMiddleware);

app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

// configure express
app.disable('x-powered-by');
app.use(bodyParser.json());
app.use(cors());

app.use('/public', express.static(__dirname + '/public'));
app.param(regexpParam);

require('./controllers/computation-management')(app, io);
require('./controllers/computation-distribution')(app, io);
require('./controllers/dashboard')(app, io);

var port = require('./config').port;
server.listen(port);
server.on("listening", function () {
    dataDao.cancelAllComputations().then(function (canceled) {
        console.log("Computations canceled: " + canceled);
    });
});
console.log("Server litening on: %d", port);