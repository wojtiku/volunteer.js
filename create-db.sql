CREATE TABLE computation
(
  id SERIAL NOT NULL,
  code text NOT NULL,
  started timestamp default now(),
  CONSTRAINT pk_computation PRIMARY KEY (id)
);

CREATE TYPE computation_status AS ENUM ('new', 'computing', 'failed', 'done');
CREATE TABLE data
(
  id SERIAL NOT NULL,
  computation_id integer NOT NULL,
  data text,
  result text,
  status computation_status NOT NULL DEFAULT 'new',
  duration double precision DEFAULT 0,
  sent timestamp,
  received timestamp,
  CONSTRAINT pk_data PRIMARY KEY (id),
  CONSTRAINT fk_data FOREIGN KEY (computation_id)
      REFERENCES computation(id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
);


-- CREATE TABLE session (
--   sid varchar NOT NULL COLLATE "default",
--   sess json NOT NULL,
--   expire timestamp(6) NOT NULL
-- )
-- WITH (OIDS=FALSE);
-- ALTER TABLE session ADD CONSTRAINT session_pkey PRIMARY KEY (sid) NOT DEFERRABLE INITIALLY IMMEDIATE;