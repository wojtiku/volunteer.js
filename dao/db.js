var db = require('knex')({
    client: 'pg',
    connection: require('./../config').dbUrl
});

module.exports = db;