var helpers = require('./helpers');
var db = require('./db');
var query = helpers.query;
var computation = require('./computation');

module.exports = {
    /**
     * Get data for computation
     */
    get: function getData(computationId) {
        return computation.get(computationId).then(function computationExists(computation) {
            if (computation.id) {
                return db("data")
                    .select("*")
                    .where("computation_id", computationId)
                    .orderByRaw('id ASC');
            } else {
                throw "Invalid computation id";
            }
        })
        .then(helpers.getFirst);
    },
    /**
     * Cancel all computations that are in progress
     */
    cancelAllComputations: function cancelAllComputations() {
        return db("data")
            .update({
                status: "new",
                result: ""
            })
            .where({
                status: 'computing'
            });
    },
    /**
     * Cancel computation already that is in progress
     */
    cancelComputation: function cancelComputation(id) {
        return db("data")
            .update({
                status: "new",
                result: ""
            })
            .where({
                id: id,
                status: 'computing'
            })
            .returning("*")
            .then(helpers.getFirst);
    },
    /**
     * Get single new data entry
     */
    getNewForComputation: function getNewForComputation() {
//        return db.raw('update data ' +
//            'set status=\'computing\' ' +
//            'where id = (' +
//                    'select id from data where status = \'new\' and pg_try_advisory_xact_lock(id) limit 1 for update' +
//                ') and status = \'new\' ' +
//            'returning *')
//            .then(helpers.getFirst);
        return db.transaction(function(trx) {
            var firstNew = trx('data')
                .select('id')
                .where('status', 'new')
                .whereRaw('pg_try_advisory_xact_lock(id)')
                .forUpdate()
                .limit(1);
            return trx("data")
                .update({
                    status: "computing",
                    sent: db.raw("now()")
                })
                .where('id', 'in', firstNew)
                .where('status', 'new')
                .returning("*");
        })
        .then(helpers.getFirst);
    },
    save: function save(status, dataId, result, duration) {
        return db("data")
            .update({
                status: status,
                result: JSON.stringify(result),
                duration: duration,
                received: db.raw("now()")
            })
            .where("id", dataId)
            .returning("*")
            .then(helpers.getFirst)
    },
    /**
     * Set data status
     */
    saveResult: function saveResult(dataId, result, duration) {
        return this.save("done", dataId, result, duration);
    },
    /**
     * Set data status
     */
    saveError: function saveError(dataId, error, duration) {
        return this.save("failed", dataId, error, duration);
    },
    /**
     * Create new data for computation
     */
    create: function createDataForComputation(computationId, data) {
        return computation.get(computationId)
            .then(function computationExists(computation) {
                if (computation.id) {
                    return db('data')
                        .insert(data.map(function(el) {
                            return {
                                computation_id: computationId,
                                data: JSON.stringify(el)
                            }
                        }))
                        .returning("id");
                } else {
                    throw "Invalid computation id";
                }
            })
            .then(helpers.getFirst);
    }
};