var eventBus = require('./../events/bus');
var helpers = require('./helpers');
var db = require('./db');
var pg = require ('pg.js');
var client = new pg.Client(require('./../config').dbUrl);

module.exports = {
    /**
     * Get stats for recent computations
     */
    getNumberOfComputations: function getNumberOfComputations() {
        return db("computation").count("*")
            .then(helpers.getFirst)
            .then(function getCount(result) {
                return result.count;
            });
    },
    /**
     * Get stats for recent computations
     */
    getStatsForRecentComputations: function getStatsForRecentComputations() {
        return db.raw(
            'select ' +
            '    computation.id, ' +
            '    computation.code, ' +
            '    computation.started, ' +
            '    max(data.received) as finished, ' +
            '    sum(data.duration) as duration, ' +
            '    avg(data.duration) as avg_duration, ' +
            '    max(data.duration) as max_duration, ' +
            '    min(data.duration) as min_duration, ' +
            '    count(data.id) as count, ' +
            '    count(case when status = \'new\' then 1 else null end) as new_count, ' +
            '    count(case when status = \'computing\' then 1 else null end) as computing_count, ' +
            '    count(case when status = \'done\' then 1 else null end) as done_count, ' +
            '    count(case when status = \'failed\' then 1 else null end) as failed_count ' +
            'from computation ' +
            'join data on computation.id = data.computation_id ' +
            'group by computation.id ' +
            'order by computation.id desc ' +
            'limit 8;'
        )
            .then(function(result) {
                return result.rows.map(function (row) {
                    if (row.new_count > 0 || row.computing_count > 0) {
                        row.finished = null;
                    }
                    row.count = +row.count;
                    row.new_count = +row.new_count;
                    row.computing_count = +row.computing_count;
                    row.done_count = +row.done_count;
                    row.failed_count = +row.failed_count;
                    return row;
                });
            });
    },
    /**
     * Get a single computation by id
     */
    get: function getComputation(id) {
        if (typeof id === "number") {
            return db("computation")
                .select("*")
                .where("id", id)
                .then(helpers.getFirst);
        } else if (typeof id === "undefined") {
            return db("computation")
                .select("*");
        } else {
            throw "Invalid computation id";
        }
    },
    /**
     * Create new computation
     */
    create: function createComputation(code) {
        return db("computation")
            .insert({ code: code.toString() })
            .returning("id")
            .tap(function() {
                setTimeout(function() {
                    eventBus.emit("computations");
                }, 0);
            })
            .then(helpers.getFirst);
    },
    /**
     * Remove computation
     */
    remove: function removeComputation(id) {
        return db('computation')
            .where('id', id)
            .del();
    },
    /**
     * Check if computation is ready to fetch the results
     */
    isReady: function isReady(id) {
        return this.get(id)
            .then(function computationExists(computation) {
                if (computation.id) {
                    return db("data")
                        .count("* as incomplete")
                        .where("computation_id", computation.id)
                        .where("status", "!=", "done")
                        .where("status", "!=", "failed");
                } else {
                    throw "Invalid computation id";
                }
            })
            .then(helpers.getFirst);
    },
    /**
     * Get results for a computation
     */
    getResults: function getResults(id) {
        return this.get(id)
            .then(function computationExists(computation) {
                if (computation.id) {
                    return db("data")
                        .select("data", "status", "result")
                        .where("computation_id", computation.id)
                        .orderByRaw('id asc');
                } else {
                    throw "Invalid computation id";
                }
            });
    }
};