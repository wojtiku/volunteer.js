

var query = require('pg-query');
query.connectionParameters = require('./../config').dbUrl;

function getFirst(array) {
    return array[0];
}

function dbError(err) {
    console.error("Error on DB query:\n", err);
}

module.exports = {
    query: query,
    getFirst: getFirst,
    dbError: dbError
}