Volunteer.js allows to make computations in users' browsers using the power of WebSockets and WebWorkers.

You will need the database set-up first. Just create DB on your server and run `create-db.sql` on it. Then adjust `config.js` to point to your DB.

To run the project, simply `npm install` all the needed packages and run it with `node index.js`.