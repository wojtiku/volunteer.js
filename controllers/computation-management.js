var eventBus = require('./../events/bus');
var when = require('when');
var computationManager = require('./../dao/computation');
var dataManager = require('./../dao/data');
var disableCache = require('./../lib/no-cache');

module.exports = function (app, io) {
    app.param('id', /^\d+$/);

    function create500Response(res) {
        return function (err) {
            res.status(500).json(err.toString()).end();
        }
    }

    app.get('/rest/computation', disableCache, function(req, res) {
        computationManager.get()
            .then(function(result) {
                res.json(result);
            })
            .catch(create500Response(res));
    });

    app.post('/rest/computation', disableCache, function(req, res) {
        computationManager.create(req.body.code)
            .then(function(result) {
                res.json(result);
            })
            .catch(create500Response(res));
    });

    app.get('/rest/computation/:id', disableCache, function(req, res) {
        var computationId = +req.params.id;
        function respondIfReady() {
            computationManager.isReady(computationId)
                .then(function isReady(result) {
                    var incomplete = +result.incomplete;
                    if (incomplete === 0) {
                        computationManager.getResults(computationId)
                            .then(function resultsFetched(results) {
                                res.json(results).end();
                            });
                    } else {
                        setTimeout(respondIfReady, Math.max(incomplete * 100, 3000));
                        //res.status(202).json({pending: incomplete});
                    }
                })
                .catch(create500Response(res));
        }
        respondIfReady();

    });

    app.post('/rest/computation/:id', disableCache, function(req, res) {
        dataManager.create(+req.params.id, req.body)
            .then(function(ids) {
                res.end();
            })
            .catch(create500Response(res));
    });
};
