var computationManager = require('./../dao/computation');
var dataManager = require('./../dao/data');
var eventBus = require('./../events/bus');

module.exports = function volounteer(app, io) {

    app.get('/',  function(req, res) {
        res.render("dashboard");
    });

    app.get('/dashboard/recent-computations',  function(req, res) {
        computationManager.getStatsForRecentComputations()
            .then(function(result) {
                res.json(result);
            })
            .catch(function(err) {
                res.status(500).json(err.toString()).end();
            });
    });

    var dashboardIo = io.of('/dashboard');
    dashboardIo.on('connection', function(socket) {
        console.log('Dashboard user connected');

        var updateTimeout
        function pushUpdatesToClient() {
            computationManager.get();
            updateTimeout = setTimeout(pushUpdatesToClient, 15000);
        }

        function emitNumberOfCurrentComputations() {
            computationManager.getNumberOfComputations().then(function(computations) {
                socket.emit("computations", computations);
            });
        }
        eventBus.on("computations", emitNumberOfCurrentComputations);
        eventBus.emit("computations");

        function emitConnectedWorkers(number) {
            socket.emit("connected-workers", number);
        }
        eventBus.on("connected-workers", emitConnectedWorkers);
        eventBus.emit("request:connected-workers");

        function emitCurrentlyComputedTasks(number) {
            socket.emit("currently-computed-tasks", number);
        }
        eventBus.on("currently-computed-tasks", emitCurrentlyComputedTasks);
        eventBus.emit("request:currently-computed-tasks");

        pushUpdatesToClient();

        socket.on('disconnect', function() {
            eventBus.removeListener("connected-workers", emitConnectedWorkers);
            eventBus.removeListener("currently-computed-tasks", emitCurrentlyComputedTasks);
            clearTimeout(updateTimeout);
            console.log('Dashboard user disconnected');
        });
    });
};