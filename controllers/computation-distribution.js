var computationManager = require('./../dao/computation');
var dataManager = require('./../dao/data');
var eventBus = require('./../events/bus');

var c = require('cli-color');
function colorId(id) {
    return c.xterm(parseInt(id.slice(-5).split("").reverse().join("").toLowerCase(),26)%256)(id);
}

function setTaskTimeout(taskId, socketId) {
    return setTimeout(function() {
        dataManager.cancelComputation(taskId).done(function() {
            decrementCurrentComputations();
            console.log('Computation timeout for %s by ' + colorId(socketId), taskId);
        });
    }, 60000); // 1 minute
}

var currentlyComputedTasks = 0;
function incrementCurrentComputations() {
    currentlyComputedTasks += 1;
    eventBus.emit("currently-computed-tasks", currentlyComputedTasks);
}

function decrementCurrentComputations() {
    currentlyComputedTasks -= 1;
    if (currentlyComputedTasks < 0) {
        currentlyComputedTasks = 0;
    }
    eventBus.emit("currently-computed-tasks", currentlyComputedTasks);
}
eventBus.on('request:currently-computed-tasks', function() {
    eventBus.emit("currently-computed-tasks", currentlyComputedTasks);
});

var connectedWorkers = 0;
eventBus.on('request:connected-workers', function() {
    eventBus.emit("connected-workers", connectedWorkers);
});

module.exports = function volounteer(app, io) {

    app.get('/compute',  function(req, res) {
        res.render("compute");
    });

    var computeIo = io.of('/compute');
    computeIo.on('connection', function(socket) {
        connectedWorkers += 1;
        eventBus.emit("connected-workers", connectedWorkers);

        console.log('Connected ' + colorId(socket.id));
        scheduleNextTaskForComputation();

        function scheduleNextTaskForComputation() {
            if (!socket.connected) return;
            if (socket.currentTask) return;
            clearTimeout(socket.currentTaskTimeout);
            clearTimeout(socket.scheduleTimeout);
            dataManager.getNewForComputation().then(function(task) {
                if (task) {
                    console.log('Task %s sent to ' + colorId(socket.id), task.id);
                    socket.currentTask = task.id ;
                    socket.emit("task", task);
                    incrementCurrentComputations();
                    socket.currentTaskTimeout = setTaskTimeout(task.id, socket.id);
                } else {
                    socket.scheduleTimeout = setTimeout(scheduleNextTaskForComputation, 1000);
                }
            });
        }

        socket.on("get-computation", function onGetComputation(id) {
            console.log('Computation %s requested by ' + colorId(socket.id), id);
            computationManager.get(+id).done(function onComputationExists(computation) {
                console.log('Computation %s sent to ' + colorId(socket.id), id);
                socket.emit("computation", computation);
            });
        });

        socket.on("res", function onNewResult(data) {
            dataManager.saveResult(data.id, data.result, data.duration).done(function onResultSaved() {
                decrementCurrentComputations();
                console.log('Result for %s received from ' + colorId(socket.id), data.id);
                delete socket.currentTask;
                scheduleNextTaskForComputation();
            });
        });

        socket.on("err", function onNewResult(data) {
            dataManager.saveError(data.id, data.error, data.duration).done(function onResultSaved() {
                decrementCurrentComputations();
                console.log('Error for %s received from ' + colorId(socket.id), data.id);
                delete socket.currentTask;
                scheduleNextTaskForComputation();
            });
        });

        socket.on('disconnect', function() {
            connectedWorkers -= 1;
            eventBus.emit("connected-workers", connectedWorkers);

            console.log('Disconnected ' + colorId(socket.id));
            if (socket.currentTask) {
                var task = socket.currentTask;
                dataManager.cancelComputation(task).done(function() {
                    decrementCurrentComputations();
                    console.log('Computation canceled for %s by ' + colorId(socket.id), task);
                });
            }
            delete socket.currentTask;
            clearTimeout(socket.scheduleTimeout);
        });
    });
};